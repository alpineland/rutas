# rutas (ルーター s)

> Routers for SolidJS & Svelte

## Modules

| Modules                              | Version                |
| ------------------------------------ | ---------------------- |
| [solid-ruta](./modules/solid-ruta)   | ![solid-ruta-version]  |
| [svelte-ruta](./modules/svelte-ruta) | ![svelte-ruta-version] |

[solid-ruta-version]: https://img.shields.io/npm/v/solid-ruta
[svelte-ruta-version]: https://img.shields.io/npm/v/svelte-ruta

## Contribution

Refer to [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

[MIT](./LICENSE)
